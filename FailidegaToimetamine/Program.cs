﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FailidegaToimetamine
{
    class Program
    {

        static string FailiNimi = @"..\..\app.config";
        static string FailKirjutamiseks = @"..\..\henntegi.txt";
        static string FailKatsetamiseks = @"..\..\katsetus.bin";
        static string FailUusAsi = @"..\..\UusAsi.txt";

        static string KohalikKaust = @"..\..";

        // räägin failidega toimetamisest peale lõunat siis
        static void Main(string[] args)
        {

            // System.IO sisaldab terve hulga failidega majandamise asju
            // jagame nad mõttes kolmeks

            // Info - FileInfo, DirInfo, DriveInfo, FilesystemInfo ja Path
            // nende klassidega saab lugeda infot kettalt failide olemasolu ja suuruse kohta
            // ja ka muid faili atribuute

            // klassi directoryinfo võimlaik kasutamine
            DirectoryInfo di = new DirectoryInfo(KohalikKaust);
            Console.WriteLine($"kaustas {di.FullName} on alamkaustad:");
            foreach(var x in di.EnumerateDirectories())
                Console.WriteLine(x.Name);

            // nii saab tekitada uusi kaustu (tekib uus kaust)
            DirectoryInfo uusDir = di.CreateSubdirectory("alamkaust");

            // ja nii olemasolevaid kustutada
            uusDir.Delete();

            // nii on võimalik lugeda üles failid mingis kaustas
            foreach(var f in di.EnumerateFiles())
                Console.WriteLine($"fail {f.Name} suurusega {f.Length} loodud {f.CreationTime}" );

            // näide kuidas kokku lugeda peidetud failid
            Console.WriteLine("Peidetud failid:");
            di.EnumerateFiles().Where(f => (f.Attributes & FileAttributes.Hidden) == FileAttributes.Hidden)
                .ToList().ForEach(f => Console.WriteLine(f.Name));

            // näide kuidas peidetud failid peidust välja tuua
            Console.WriteLine("toome peidetud failid peidust välja");
            di.EnumerateFiles().Where(f => (f.Attributes & FileAttributes.Hidden) == FileAttributes.Hidden)
                .ToList().ForEach(f => f.Attributes ^= FileAttributes.Hidden);

            // näide kuidas muuta näiteks faili loomisaega
            FileInfo ffi = new FileInfo(FailiNimi);
            ffi.CreationTime = DateTime.Now.AddYears(0);

            // töö PATH klassi staatiliste meetoditega

            string test = "";
            Console.WriteLine(test = Path.Combine(@"c:\" , @"test\veel", "failinimi.txt"));
            Console.WriteLine(Path.ChangeExtension(test, ".bak"));
            Console.WriteLine(Path.GetDirectoryName(test));
            Console.WriteLine(Path.GetFileName(test));



            string failinimi2 = "";
            // faili kopeerimine, ümbernimetamine ja kustutamine
            File.Copy(FailiNimi, failinimi2 = Path.ChangeExtension(FailUusAsi, "copy"));
            File.Move(failinimi2, failinimi2 = Path.ChangeExtension(FailUusAsi, "bak"));
            File.Delete(failinimi2);

            Console.WriteLine();
            // Striimid ehk jadad
            // FileStream - tekstijada
            // BinaryStream - baidijada

            // Readerid-writerid
            // StreamReader/Writer - tekstijadasse kirjutaja-lugeja
            // Binaryreader/Writer - binaryjadasse kirjutaja-lugeja

            // no sina vahepeale ka File klass on meetoditega

            // loeb etteantud tekstifaili stringimassiiviks (eeldus - reavahetused)
            var x1 = File.ReadAllLines(FailiNimi);

            //foreach (var y in x1) Console.WriteLine(y);

            // loeb etteantud faili stringiks
            var x2 = File.ReadAllText(FailiNimi);

            // loeb etteantud faili rida haaval kuni otsa saab
            foreach(var y in File.ReadLines(FailiNimi))
                Console.WriteLine(y);

            // sama aga streamreaderiga - sel on ReadLine() meetod
            using (StreamReader sr = new StreamReader(FailiNimi))
            {
                while (sr.Peek() != -1)
                    Console.WriteLine(sr.ReadLine());
            }

            // faili salvestamine StreamWriteri ja FileStreamiga
            using (FileStream fs = new FileStream(FailKirjutamiseks, FileMode.Append, FileAccess.Write))
            using (StreamWriter sw = new StreamWriter(fs))
            {
                string[] nimed = { "Joosp", "Juku", "Jaanus" };
                foreach (var nimi in nimed) sw.WriteLine(nimi);
            }

            // failist (baidijadast) int arvu lugemine
            using (FileStream fs = new FileStream(FailKirjutamiseks, FileMode.Open, FileAccess.Read))
            using (BinaryReader br = new BinaryReader(fs))
            {
                int imelik = br.ReadInt32();
                Console.WriteLine(imelik);
            }

            // faili täisarvude jada salvestamine (binary)
            using (FileStream fs = new FileStream(FailKatsetamiseks, FileMode.Create, FileAccess.Write))
            using (BinaryWriter bw = new BinaryWriter(fs))
            {
                int[] arvud = { 1, 2, 3, 256, 65536};
                foreach (var a in arvud)
                    bw.Write(a);
                
            }

            // faili sisu lugemine ja kuvamine 16-arvudena
            byte[] sisu = File.ReadAllBytes(FailKatsetamiseks);
            Console.WriteLine(sisu.Length);

            for (int i = 0; i < sisu.Length; i++)
            {
                var b = sisu[i];
                Console.Write($"{(i % 32 == 0 ? "\n" : i % 8 == 0 ? " " : "")}{b:X2}");
            }
            Console.WriteLine();




        }
    }



   

}
